#pragma once

//
//  Message.h
//

#ifndef Message_h
#define Message_h

#include <string>

namespace common {
	namespace protocol {
		namespace pitch {
			class Message {
			public:
				typedef std::pair<std::string::const_iterator, unsigned long> Item;

				static const unsigned short MessageMinLen = 27;

				static const unsigned short MessageTypeOffset = 8;
				static const unsigned short OrderIdOffset = 9;
				static const unsigned short OrderIdLen = 12;
				static const unsigned short SymbolLen = 6;

				static const unsigned short SiteIndicator = 21;

				typedef char Type;
				typedef unsigned long Shares;

				explicit Message(const std::string& message) : mBuffer(message) {
					if ((message.size() < MessageMinLen)) {
						throw std::invalid_argument("Message must be at last 27 characters long");
					}
				}

				/// prevent copy and assignment
				Message(const Message& rhs) = delete;
				Message& operator=(const Message& rhs) = delete;

				virtual ~Message() {
				}

				virtual char getType() const {
					return mBuffer[MessageTypeOffset];
				}

				const std::string getOrderId() const {
					return std::string(&mBuffer[Message::OrderIdOffset], OrderIdLen);
				}

				virtual unsigned long getShares() const {
					static const unsigned short offset = 22;
					static const unsigned short len = 6;

					return std::stoul(getBufferSlice(offset, len));
				}

				virtual std::string getSymbol() const {
					static const unsigned short offset = 28;
					static const unsigned short len = 6;

					return getBufferSlice(offset, len);
				}

			protected:
				std::string mBuffer;

				std::string getBufferSlice(unsigned short offset, unsigned short len) const {
					return std::string(&mBuffer[offset], len);
				}
			};
		} 
	} 
} 

#endif



