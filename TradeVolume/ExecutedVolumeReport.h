#pragma once

//
//  ExecutedVolumeReport.h
//

#ifndef ExecutedVolumeReport_h
#define ExecutedVolumeReport_h

#include <algorithm>
#include <exception>
#include <iostream>
#include <vector>
#include <unordered_map>

#include "Report.h"
#include "MessageFactory.h"

namespace common {
	namespace util {

		/// Produces report of all executed orders showing n executed orders with most volume
		/// sorted from highest to lowest
		class ExecutedVolumeReport : public Report {
		public:
			static const std::size_t DefaultReportSize = 10;

			typedef std::pair<std::string, unsigned long> Pair;

			/// to store all AddOrders, key = orderIid
			typedef Pair StockSymbolSharesPair;
			typedef std::unordered_map<std::string, StockSymbolSharesPair> OrderAddMap;

			/// to store all Executed
			typedef Pair OrderIdSharesPair;
			typedef std::vector<OrderIdSharesPair> OrderExecutedVector;


			/// to store final data i.e. stock symbol and executed volume
			/// key = stock symbol
			typedef std::unordered_map<std::string, unsigned long> ExecutedVolumeMap;

			ExecutedVolumeReport(std::size_t count = DefaultReportSize) : mCount(count) {
				checkCount();
			}

			void operator()(const std::string& message);

			void operator()();

		private:
			std::size_t mCount;
			OrderAddMap mOrderAdd;
			OrderExecutedVector mOrderExecuted;
			mutable ExecutedVolumeMap mExecutedVolume;

			void checkCount() {
				if (mCount < 1) {
					throw std::invalid_argument("report count entry must be at least 1");
				}
			}

			struct SortVolume {
				bool operator()(const Pair& left, const Pair& right) {
					return left.second > right.second;
				}
			};

			/// This method creates entities needed by a report and places them
			/// into predefined container based on message type. It runs in O(n)
			/// where n is the number of messages.
			/// Only messages: OrderAdd, OrderExecuted, Trade, TradeLong are considered
			/// and all other messages types are ignored.
			void aggregate(const std::string& buffer);

			/// This method finalizes a report by matching executed orders against
			/// AddOrders to get stock symbol. Note each lookup into mOrderAdd runs
			/// in O(1) on average as this is a lookup into a hash map.
			void finalize();
		};


	} /// namespace util
} /// namespace common



#endif

