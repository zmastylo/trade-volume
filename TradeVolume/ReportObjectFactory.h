#pragma once

#include <memory>
#include <string>

#include "ExecutedVolumeReport.h"
#include "Report.h"

using namespace common::util;
class ReportObjectFactory {
public:
	const std::string exec_vol = "exec_vol";

	std::shared_ptr<Report> create(const std::string& reportName, std::size_t count = 10) {
		if (reportName == exec_vol) {
			return std::shared_ptr<Report>(new ExecutedVolumeReport(count));
		}
		// other reports

		throw std::invalid_argument("No report defined for: " + reportName);
	}
};
