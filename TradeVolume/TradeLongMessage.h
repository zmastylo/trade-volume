#pragma once

#include <string>
#include "Message.h"

namespace common {
	namespace protocol {
		namespace pitch {

			class TradeLongMessage : public Message {
			public:
				explicit TradeLongMessage(const std::string& message) : Message(message) {
				}

				std::string getSymbol() const {
					static const unsigned short offset = 28;
					static const unsigned short len = 8;

					return getBufferSlice(offset, len);
				}
			};
		}
	}
}
