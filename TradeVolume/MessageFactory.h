#pragma once

//
//  MessageParser.hpp
//  

#ifndef MessageParser_h
#define MessageParser_h

#include <memory>
#include <string>

#include "IgnoredMessage.h"
#include "OrderAddLongMessage.h"
#include "OrderAddMessage.h"
#include "OrderExecutedMessage.h"
#include "TradeMessage.h"
#include "TradeLongMessage.h"

namespace common {
	namespace protocol {
		namespace pitch {

			/// MessageParser knows how to parse fields from
			/// a given pitch message in std::string format
			/// It can be further customized to parse all necessary
			/// fields to create pitch messages like AddOrder, Trade etc.
			class MessageFactory {
			public:
				static const unsigned char OrderExecuted = 'E';
				static const unsigned char OrderAdd = 'A';
				static const unsigned char OrderAddLong = 'd';
				static const unsigned char Trade = 'P';
				static const unsigned char TradeLong = 'r';

				static std::shared_ptr<Message> createMessage(const std::string& message) {
					auto messageType = message[Message::MessageTypeOffset];
					switch (messageType) {
					case Trade:
						return std::shared_ptr<Message>(new TradeMessage(message));
					case OrderAdd:
						return std::shared_ptr<Message>(new OrderAddMessage(message));
					case OrderAddLong:
						return std::shared_ptr<Message>(new OrderAddLongMessage(message));
					case TradeLong:
						return std::shared_ptr<Message>(new TradeLongMessage(message));
					case OrderExecuted:
						return std::shared_ptr<Message>(new OrderExecutedMessage(message));
					default:
						return std::shared_ptr<Message>(new IgnoredMessage(message));
					}
				}
			};

		} /// namespace pitch
	} /// namespace protocol
} /// namespace common



#endif

