#include "stdafx.h"

namespace common {
	namespace util {
		void ExecutedVolumeReport::operator()(const std::string& message) {
			aggregate(message);
		}

		void ExecutedVolumeReport::operator()() {
			finalize();
		}

		void ExecutedVolumeReport::aggregate(const std::string& buffer) {
			auto message = common::protocol::pitch::MessageFactory::createMessage(buffer);
			switch (message->getType()) {
			case common::protocol::pitch::MessageFactory::OrderAdd:
			{
				auto orderId = message->getOrderId();
				mOrderAdd[orderId] = std::make_pair(message->getSymbol(), message->getShares());
				break;
			}

			case common::protocol::pitch::MessageFactory::OrderExecuted:
			{
				auto orderId = message->getOrderId();
				mOrderExecuted.emplace_back(std::make_pair(orderId, message->getShares()));
				break;
			}

			case common::protocol::pitch::MessageFactory::Trade:
			{
				auto symbol = message->getSymbol();
				mExecutedVolume[symbol] += message->getShares();
				break;
			}
			}
		}

		void ExecutedVolumeReport::finalize() {
			std::for_each(std::begin(mOrderExecuted), std::end(mOrderExecuted),
				[&](const OrderIdSharesPair & item) {
				auto iter = mOrderAdd.find(item.first);
				if (iter != std::end(mOrderAdd)) {
					mExecutedVolume[iter->second.first] += (item.second + iter->second.second);
				}
			});

			if (mOrderExecuted.size() < 1) {
				std::cout << "No executed orders" << std::endl;
				return;
			}

			// This shall get optimized by a c++ compiler (run with -std=c++11) so
			// no copies will be involved (move semantics will take place).
			std::vector<Pair> v(std::begin(mExecutedVolume), std::end(mExecutedVolume));

			// Find nth element i.e. an element such that for every element
			// behind nth element passed functor returns false. It runs O(begin(v) + mCount).
			auto end = v.size() <= mCount ? v.end() : v.begin() + mCount;
			std::nth_element(std::begin(v), end, std::end(v), SortVolume());

			// Now we sort our vector partially. This runs in O(mCount * log mCount)
			std::sort(std::begin(v), end, SortVolume());

			// Finally we show the result!
			std::for_each(std::begin(v), end, [](const Pair & item) {
				std::cout << item.first << "  " << item.second << std::endl;
			});
		}
	}
}