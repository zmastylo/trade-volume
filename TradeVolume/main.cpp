//
//  main.cpp
//  
#include "stdafx.h"
#include "ExecutedVolumeReport.h"
#include "ReportObjectFactory.h"
#include "ReportRunner.h"

using namespace common::util;
int main(int argc, const char* argv[]) {
	unsigned short count(10);

	if (argc < 3) {
		std::cout << "Need report name and trade data file as parameters" << std::endl;
		return 0;
	}

	try {
		const std::string reportName(argv[1]);
		auto report = ReportObjectFactory().create(reportName);

		const std::string fileName(argv[2]);
		ReportRunner::hint hint = ReportRunner(*report)(fileName);
		std::cout << hint.second << std::endl;
	}
	catch (std::exception & e) {
		std::cout << e.what() << std::endl;
	}
	catch (...) {
		std::cout << "Unhandled exception" << std::endl;
	}

	return 0;
}

