#pragma once

//
//  Report.hpp
//

#ifndef Report_hpp
#define Report_hpp

namespace common {
	namespace util {

		class Report {
		public:
			/// aggregate pitch messages
			virtual void operator()(const std::string& message) = 0;

			/// run report
			virtual void operator()() = 0;
		};

	} /// namespace util
} /// namespace common



#endif

