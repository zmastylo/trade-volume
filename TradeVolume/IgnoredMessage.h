#pragma once

#include <string>
#include "Message.h"

namespace common {
	namespace protocol {
		namespace pitch {
			class IgnoredMessage : public Message {
			public:
				explicit IgnoredMessage(const std::string& message) : Message(message) {
				}

				unsigned long getShares() const {
					throw new std::exception("Ignored message");
				}

				std::string getSymbol() const {
					throw new std::exception("Ignored message");
				}
			};
		}
	}
}
