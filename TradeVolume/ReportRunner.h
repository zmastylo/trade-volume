#pragma once

#include <algorithm>
#include <iterator>
#include <iostream>
#include <fstream>
#include <memory>
#include <string>

#include "Report.h"

namespace common {
	namespace util {
		class ReportRunner {
		public:
			typedef std::pair<bool, std::string> hint;
			typedef std::istreambuf_iterator<char> iterator;

			ReportRunner(Report& report) 
				:mReport(report) {
			}

			hint operator()(const std::string& fileName) {
				std::ifstream file(fileName);
				return file.is_open() ? handleReport(file) : handleError(fileName);
			}

		private:
			Report& mReport;

			hint handleError(const std::string& fileName) {
				std::string errorMsg("Error opening file:" + fileName);
				return hint(false, errorMsg);	
			}

			hint handleReport(std::istream& stream) {
				std::string message;
				std::for_each(iterator(stream), iterator(), [&](const char c) {
					message += c;
					if (c == '\n') {
						mReport(message);
						message.clear();
					}
				});

				mReport();
				return hint(true, "----- Report Successful -----");
			}
		};
	}
}
