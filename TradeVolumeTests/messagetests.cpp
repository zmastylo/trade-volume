#include "stdafx.h"
#include "CppUnitTest.h"

#include "..\TradeVolume\Message.h"
#include "..\TradeVolume\MessageFactory.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace common::protocol;

namespace TradeVolumeTests {		
	TEST_CLASS(MessageTests) {
	public:
		TEST_METHOD(TestVerifyConstructorThrows) {
			std::string buffer("Bad message");
			auto isThrown = false;
			try {
				auto message = pitch::MessageFactory::createMessage(buffer);
			}
			catch (std::exception) {
				isThrown = true;
			}
			Assert::IsTrue(isThrown);
		}

		TEST_METHOD(TestMessageGetOrderId) {
			// TODO: Your test code here
			std::string buffer("28800011AAK27GA0000DTS000100SH    0000619200Y");
			auto message = pitch::MessageFactory::createMessage(buffer);
			const std::string expectedValue = "AK27GA0000DT";
			auto actualValue = message->getOrderId();
			Assert::AreEqual(expectedValue, actualValue, L"message", LINE_INFO());
		}

		TEST_METHOD(TestAddOrderMessageGetType) {
			// TODO: Your test code here
			std::string buffer("28800011AAK27GA0000DTS000100SH    0000619200Y");
			auto message = pitch::MessageFactory::createMessage(buffer);
			char expectedValue = pitch::MessageFactory::OrderAdd;
			auto actualValue = message->getType();
			Assert::AreEqual(expectedValue, actualValue, L"message", LINE_INFO());
		}

		TEST_METHOD(TestAddOrderMessageGetShares) {
			std::string buffer("28800011AAK27GA0000DTS000100SH    0000619200Y");
			auto message = pitch::MessageFactory::createMessage(buffer);
			unsigned long expectedValue = 100;
			auto actualValue = message->getShares();
			Assert::AreEqual(expectedValue, actualValue, L"message", LINE_INFO());
		}

		TEST_METHOD(TesAddOrderMessagetGetSymbol) {
			std::string buffer("28800011AAK27GA0000DTS000100SH    0000619200Y");
			auto message = pitch::MessageFactory::createMessage(buffer);
			std::string expectedValue = "SH    ";
			auto actualValue = message->getSymbol();
			Assert::AreEqual(expectedValue, actualValue, L"message", LINE_INFO());
		}

		TEST_METHOD(TestGetSharesSame) {
			// message type AddOrder
			std::string buffer("28800011AAK27GA0000DTS000100SH    0000619200Y");
			auto addOrderShares = pitch::MessageFactory::createMessage(buffer)->getShares();

			// message type Trade
			buffer = "28800011PAK27GA0000DTS000100SH    0000619200Y";
			auto tradeShares = pitch::MessageFactory::createMessage(buffer)->getShares();
			Assert::AreEqual(addOrderShares, tradeShares, L"message", LINE_INFO());

			// message type TradeLong
			buffer = "28800011rAK27GA0000DTS000100SH    0000619200Y";
			auto tradeLongShares = pitch::MessageFactory::createMessage(buffer)->getShares();
			Assert::AreEqual(tradeShares, tradeLongShares, L"message", LINE_INFO());
		}

		// And so on .....
	};
}