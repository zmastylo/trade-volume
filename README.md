# Run report
.\TradeVolume.exe report_type file_path
where:
	- report_type :report type and currently the supported values is: exec_vaol
	- file_path   :full file path contating exchange trade data

# Info
The program creates a report of up to the top ten symbols by executed volume. 
Need to read Add Order messages and remember what orders are open and 
apply Order Cancel and Order Executed messages. 

Trade Messages are sent for orders which were hidden. 
Need to use Order Executed and Trade Messages to compute total volume. 
May ignore any Trade Break and long messages (�B�, �r�, �d�).Each line
of the output has a ticker and traded volume for that ticker. 

// Trade message, OrderAdd message, Trade long, and order executed messages
// and their representations
//
// shares -- Trade, OrderAdd, TradeLong -- offset=22, len=6
//           OrderExecuted              -- offset=21, len=6
//
// symbol -- Trade, OrderAdd            -- offset=28, len=6/8
//           TradeLong                  -- offset=28, len=8

The sample output follows:
 
SPY   24486275
QQQQ  15996041
XLF   10947444
IWM    9362518
MSFT   8499146
DUG    8220682
C      6756932
F      6679883
EDS    6673983
QID    6526201
 
# Pitch specs
The PITCH specification is available from the website:
http://www.batstrading.com/resources/membership/BATS_PITCH_Specification.pdf
 
